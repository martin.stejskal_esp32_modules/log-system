/**
 * @file
 * @author Martin Stejskal
 * @brief Debug module which deals with printing messages to some output
 */
#ifndef __LOG_SYSTEM_H__
#define __LOG_SYSTEM_H__
// ===============================| Includes |================================
// Following allow to load "global" settings from one file
// When compiler support it, check if user file exists and if yes, include it
#if defined __has_include
#if __has_include("cfg.h")
#include "cfg.h"
#endif
#endif

// Based on current platform, load appropriate header file
#if defined(ESP_PLATFORM)
#include "log_system_esp.h"
#else
#error "Can not find platform specific header and therefore platform specific"
" functions used by log system"
#endif
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __LOG_SYSTEM_H__
