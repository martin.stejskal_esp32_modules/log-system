/**
 * @file
 * @author Martin Stejskal
 * @brief Architecture specific header for ESP
 *
 * Original design was tested on ESP32, but in general, it should work for
 * other MCU as well
 */
#ifndef __LOG_SYSTEM_ESP_H__
#define __LOG_SYSTEM_ESP_H__
// ===============================| Includes |================================
#include <esp_log.h>
// ================================| Defines |================================
/**
 * @brief Log related macros
 *
 * Better to use ESP's log system with colors and timestamp, instead of
 * printf()
 */
#ifndef LOGE
#define LOGE(tag, ...) ESP_LOGE(tag, __VA_ARGS__)
#endif  // DBG_LOGE

#ifndef LOGW
#define LOGW(tag, ...) ESP_LOGW(tag, __VA_ARGS__)
#endif  // DBG_LOGW

#ifndef LOGI
#define LOGI(tag, ...) ESP_LOGI(tag, __VA_ARGS__)
#endif  // DBG_LOGI

#ifndef LOGD
#define LOGD(tag, ...) ESP_LOGD(tag, __VA_ARGS__)
#endif  // DBG_LOGD

#ifndef LOGV
#define LOGV(tag, ...) ESP_LOGV(tag, __VA_ARGS__)
#endif  // DBG_LOGV
// ==========================| Preprocessor checks |==========================

#endif  // __LOG_SYSTEM_ESP_H__
